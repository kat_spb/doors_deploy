#!/bin/bash -x

if [ -z "${GIT_BRANCH}" ]; then
    echo "ERROR: GIT_BRANCH is undefined"
    exit 1
fi

name=`date +%s`
id=`date +%Y%m%d`

VERSION_ID=`echo ${GIT_BRANCH} | sed 's|^.*/||'`

src="doors-${VERSION_ID}git${id}"

cat > changelog <<END
doors (${VERSION_ID}git${id}-1) unstable; urgency=medium

  * Initial release

 -- Ekaterina Mosseyko <ekaterina.mosseyko@compvisionsys.com>  $(date +"%a, %d %b %Y %T %z")

END
