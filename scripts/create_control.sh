#!/bin/bash -x

cat > changelog <<END
Source: doors
Section: unknown
Priority: optional
Maintainer: Ekaterina Mosseyko <ekaterina.mosseyko@compvisionsys.com>
Build-Depends: debhelper-compat (= 12), quilt
Standards-Version: 4.5.0
Homepage: https://packages.augmentedcity.com/debian/testing/doors
#Vcs-Browser: https://salsa.debian.org/debian/doors
#Vcs-Git: https://salsa.debian.org/debian/doors.git

Package: doors
Architecture: any
Depends: \${shlibs:Depends}, \${misc:Depends}
Description: Doors project
 Doors project deploy package with broken dependencies
END
