#!/bin/bash -x

if [ -z "${MARCH}" ]; then
    echo "MARCH is not defined, use -march=native -mtune=native"
    MARCH="native"
fi

cat > rules <<END
#!/usr/bin/make -f
# See debhelper(7) (uncomment to enable)
# output every command that modifies files on the build system.
export DH_VERBOSE = 1

# see FEATURE AREAS in dpkg-buildflags(1)
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
include /usr/share/dpkg/architecture.mk

CMAKE_ARCH_FLAGS :=

# see ENVIRONMENT in dpkg-buildflags(1)
# package maintainers to append CFLAGS
#export DEB_CFLAGS_MAINT_APPEND  = -Wall -pedantic -pthread
# package maintainers to append LDFLAGS
#export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

CMAKE_FLAGS = \\
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \\
    -DCMAKE_CXX_FLAGS="-O3 -march=${MARCH} -mtune=${MARCH}" \\
    -DBUILD_COLMAP_EXE=OFF \\
    -DCMAKE_INSTALL_PREFIX=/usr \\
    \$(CMAKE_ARCH_FLAGS)

%:
	dh \$@ --buildsystem=cmake

# dh_make generated override targets
# This is example for Cmake (See https://bugs.debian.org/641051 )

override_dh_clean:
	dh_clean
#	rm -rf \`find -name '.git*'\`
	rm -rf \`find -type f -name '*.o' -name '*.a' -name '*.so'\`
	rm -rf tools_server/app/__pycache__/
	rm -rf tools_server/tests/__pycache__/

	rm -rf wrappers/f10_solver_py/tests/__pycache__/
	rm -rf engine/localization_service/dependencies/visual_index/dependencies/faiss/config.log
	rm -rf engine/localization_service/dependencies/visual_index/dependencies/faiss/config.status
	rm -rf engine/localization_service/dependencies/visual_index/dependencies/faiss/makefile.inc
	rm -rf engine/reconstruction/dependencies/colmap/colmap/src/util/version.h
	cp -a ../.git .

override_dh_auto_configure:
	dh_auto_configure -- \\
	    -DCMAKE_LIBRARY_PATH=\$(DEB_HOST_MULTIARCH) \\
	    -DBUILD_TESTS=OFF \\
	    \$(CMAKE_FLAGS)

override_dh_auto_test:
	true

override_dh_auto_install:
	dh_auto_install
	rm -rf debian/doors/home
END

chmod +x rules