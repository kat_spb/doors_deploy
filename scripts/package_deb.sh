#!/bin/bash -x

if [ -z $1 ]; then
    echo "Usage: prepare_deb.sh branch path_to_source"
    exit 1
fi

if [ -z $2 ]; then
    echo "Usage: prepare_deb.sh branch path_to_source"
    exit 1
fi

branch=$1
path_to_src=$2
postfix=`date +%s`

rm -rf ${path_to_src}
GIT_LFS_SKIP_SMUDGE=1 git clone --recurse-submodules -j32 --branch ${branch} git@bitbucket.org:relative_localization/doors.git ./${path_to_src}

git clone git@bitbucket.org:relative_localization/debian.git ./debian_${postfix}

cd ${path_to_src}

## CLEAN REPOSITORY (need to check)

#delete json/metis/nghttp2-asio dependencies as code
rm -rf engine/tools/model-splitter/dependencies
#path=`pwd` && cd ../.. && res=`find -name 'metis.patch'` && cd $path
patch -p1 < ../../patches/metis.patch
rm -rf server/dependencies
patch -p1 < ../../patches/nghttp2.patch
rm -rf data_providers/dependencies/htmlsieve/dependencies
rm -rf engine/core/dependencies
rm -rf db/dependencies/json
patch -p1 < ../../patches/json.patch
rm -rf db/cmake

#rm -rf `find -name 'branches' | grep '.git'`
#rm -rf `find -name 'hooks' | grep '.git'`
#rm -rf `find -name 'objects' | grep '.git'`
#rm -rf `find -name 'logs' | grep '.git'`
#rm -rf `find -name 'lfs' | grep '.git'`

rm -rf obj-*
rm -rf `find -type f -name '*.o' -name '*.a' -name '*.so'`
rm -rf `find -type d -name '__pycache__'`
rm -rf engine/localization_service/dependencies/visual_index/dependencies/faiss/makefile.inc
rm -rf engine/localization_service/dependencies/visual_index/dependencies/faiss/config.log
rm -rf engine/localization_service/dependencies/visual_index/dependencies/faiss/config.status
rm -rf engine/reconstruction/dependencies/colmap/colmap/src/util/version.h

cd ..

pkg_name=`echo "${path_to_src}" | sed 's|doors-|doors_|'`
rm -rf "${pkg_name}*"

#pkg_src=`echo "${path_to_src}" | sed 's|doors-|doors_|'`.orig.tar.gz
pkg_src=`echo "${path_to_src}" | sed 's|doors-|doors_|'`.orig.tar.bz2

#tar czf "$pkg_src" "${path_to_src}"
tar cf "$pkg_src" --use-compress-prog=pbzip2 "${path_to_src}"

pkg_version=`echo "${path_to_src}" | sed 's|doors-||'`
d=`date +%d`
m=`date +%b`
y=`date +%Y`
nd=`date +%a`
t=`date +%T`

pwd

#update debian/changelog
echo -ne "doors (${pkg_version}-1) unstable; urgency=medium\n\n  * Initial release\n\n -- Ekaterina Mosseyko <kat@doorsserver3.vergendo.com>  $nd, $d $m $y $t +0300\n" > debian_${postfix}/changelog

rm -rf ${path_to_src}/debian
mv debian_${postfix} ${path_to_src}/debian

cd ${path_to_src}
dpkg-buildpackage -rfakeroot 2>&1 | tee ../build_${path_to_src}.log
cd ..

ls -la "${pkg_name}*"
