#!/bin/bash -x

if [ -z "$1" ]; then
    echo "Usage: $0 changelog"
    changelog_file="changelog"
else
    changelog_file="$1"
fi

cat > changelog.tmp <<END
$(head -n 1 "${changelog_file}" | sed "s/(/(1:/" | sed "s/)/skz1)/")

  * Build with CUDA and -march=native

 -- Ekaterina Mosseyko <ekaterina.mosseyko@compvisionsys.com>  $(date +"%a, %d %b %Y %T %z")

END

cat "${changelog_file}" >> changelog.tmp

mv changelog.tmp "${changelog_file}"


